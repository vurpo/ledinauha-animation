# ledinauha-animation

This program animates a LED strip through the serial port.

Made for Turku Hacklab

### Dependencies

* Arduino project at https://github.com/hacklab-turku/ledinauha

* The sensor API at https://github.com/bionik/pi_api

### Features

* Animation turns on and off depending on movement in room (using PIR sensor)

* Offline indicator