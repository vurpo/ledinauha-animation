
pub fn clamp(x: f32, min: f32, max: f32) -> f32 {
  min.max(max.min(x))
}

pub fn modulo(r: f32, l: f32) -> f32 {
  (r % l + l) % l
}
