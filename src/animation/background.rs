
use math;
use time;

pub trait PlasmaPattern {
  fn plasma_pattern(&self, x: f32) -> (f32, f32, f32);
}

pub struct Rainbow;
impl PlasmaPattern for Rainbow {
  fn plasma_pattern(&self, x: f32) -> (f32, f32, f32) {
    let x = math::modulo(x, 1.0);
    (
      math::clamp(-6.0*x+2.0, 0.0, 1.0).max(math::clamp(6.0*x-4.0, 0.0, 1.0)),
      math::clamp(6.0*x, 0.0, 1.0).min(math::clamp(-6.0*x+4.0, 0.0, 1.0)),
      math::clamp(6.0*x-2.0, 0.0, 1.0).min(math::clamp(-6.0*x+6.0, 0.0, 1.0))
    )
  }
}

pub struct RedWave;
impl PlasmaPattern for RedWave {
  fn plasma_pattern(&self, x: f32) -> (f32, f32, f32) {
    let x = math::modulo(x, 1.0);
    (math::clamp((x*31.41).sin()*0.4+0.6, 0.0, 1.0), 0.0, 0.0)
  }
}

pub struct GreenWave;
impl PlasmaPattern for GreenWave {
  fn plasma_pattern(&self, x: f32) -> (f32, f32, f32) {
    let x = math::modulo(x, 1.0);
    (0.0, math::clamp((x*31.41).sin()*0.4+0.6, 0.0, 1.0), 0.0)
  }
}

pub fn background_pattern(pattern1: &Box<PlasmaPattern>, pattern2: &Box<PlasmaPattern>, fade_factor: f32, index: u8, time: time::Duration) -> (f32, f32, f32) {
  let time = time.num_milliseconds() as f32 / 1000.0;
  
  let factor =  // This math is indecipherable without comments
    (
      ( // the moving circle
        (
          ((index as f32-25.0)-(time/3.0).cos()*25.0*0.6).powi(2) //squared X coordinate of the center point
          +
          (((time/5.0).sin()*25.0).powi(2)) //squared Y coordinate of the center point
        ).sqrt() //square root of x^2+y^2 is the distance to (x,y), this creates a "circle" effect
        /5.0
      ).sin()
      +
      ( //the linear stripe pattern
        (index as f32*0.25).cos()
        +
        (time/7.0).sin()
        *5.0
      ).sin()
    )
    *0.25-0.5; //I'm done

  let color1 = pattern1.plasma_pattern(factor);
  let color2 = pattern2.plasma_pattern(factor);

  (
    (((1.0-fade_factor)*color1.0)+(fade_factor*color2.0)).min(1.0),
    (((1.0-fade_factor)*color1.1)+(fade_factor*color2.1)).min(1.0),
    (((1.0-fade_factor)*color1.2)+(fade_factor*color2.2)).min(1.0),
  )
}


