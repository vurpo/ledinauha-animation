
use time;

pub trait Effect {
  pub fn effect(&self, time: time::Duration, x: f64) -> (u8,u8,u8,u8);
}

pub struct ShootingStar {
  velocity: f32,
  color: (u8,u8,u8),
}  

impl Effect for ShootingStar {
  pub fn effect(&self, time: time::Duration, x: f64) -> (u8,u8,u8,u8) {
    (0,0,0,0) //TODO
  }
}

//pub fn effect
