
mod background; 

use rand;
use rand::Rng;
use easer::functions::*;
use time;

use math;
pub use animation::background::*;

pub struct IntroOutroFade {
  intro_start_time: time::PreciseTime,
  intro_duration: time::Duration,
  outro_start_time: time::PreciseTime,
  outro_duration: time::Duration,

  on: bool,

  shuffled_indices: Vec<u8>,
}

impl IntroOutroFade {
  pub fn new(
    intro_start_time: time::PreciseTime,
    intro_duration: time::Duration,
    outro_start_time: time::PreciseTime,
    outro_duration: time::Duration,

    num_leds: u8) -> IntroOutroFade {

    let mut rng = rand::thread_rng();
    let mut indices: Vec<u8> = (0..num_leds).collect();
    rng.shuffle(&mut indices);

    IntroOutroFade {
      intro_start_time: intro_start_time,
      intro_duration: intro_duration,
      outro_start_time: outro_start_time,
      outro_duration: outro_duration,

      on: false,
      
      shuffled_indices: indices,
    }
  }

  pub fn get_fade(&self, index: u8, now: time::PreciseTime) -> f32 {
    //let now = time::PreciseTime::now();
    if self.on {
      return Bounce::ease_in_out(
        math::clamp((self.intro_start_time.to(now).num_milliseconds()-self.shuffled_indices[index as usize] as i64*20) as f32, 0.0, self.intro_duration.num_milliseconds() as f32),
        0.0f32,
        1.0f32,
        self.intro_duration.num_milliseconds() as f32);

    } else {
      return Bounce::ease_in_out(
        math::clamp((self.outro_start_time.to(now).num_milliseconds()-self.shuffled_indices[index as usize] as i64*20) as f32, 0.0, self.outro_duration.num_milliseconds() as f32),
        1.0f32,
        -1.0f32,
        self.outro_duration.num_milliseconds() as f32);
    }
  }

  pub fn start_intro(&mut self) {
    if !self.on {
      self.on = true;
      self.intro_start_time = time::PreciseTime::now();
    }
  }

  pub fn start_outro(&mut self) {
    if self.on {
      self.on = false;
      self.outro_start_time = time::PreciseTime::now();
    }
  }
}

pub fn strip_pattern(pattern1: &Box<PlasmaPattern>, pattern2: &Box<PlasmaPattern>, fade_factor: f32, index: u8, time: time::Duration) -> (f32, f32, f32) { 
  let background_color = background_pattern(pattern1, pattern2, fade_factor, index, time);
  //let effect_color = 
  
  background_color //TODO
}
