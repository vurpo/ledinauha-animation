
use serde_json;
use serde_json::*;

#[derive(Serialize, Deserialize)]
pub struct PirStatus {
  pub status: String,
  pub timestamp: String,
  pub time: String,
}
