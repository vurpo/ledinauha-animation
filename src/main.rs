#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]

extern crate serial;
extern crate time;
extern crate rand;
extern crate easer;
extern crate hyper;
extern crate serde;
extern crate serde_json;

mod animation;
mod math;
mod pir;

use std::io::{
  Read,
  Write,
};
use serial::prelude::*;

//use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;

const N_LEDS: u8 = 50;

enum LedStripCommand {
  TurnOn,
  TurnOff,
  ChangePalette(Box<animation::PlasmaPattern + Send>),
}

fn wait_1_byte<R: Read>(read: &mut R) {
  let mut buf = [0u8];
  while read.read(&mut buf).unwrap_or(0) < 1 {}
}

fn main() {
  let args: Vec<String> = std::env::args().collect();
  if args.len() != 2 {
    println!("Usage: ledinauha-animation <serial port device>");
    std::process::exit(64);
  }

  let start_time = time::PreciseTime::now();

  let mut serial_port = serial::open(&args[1]).unwrap();
  serial_port.reconfigure(&|settings| {
    try!(settings.set_baud_rate(serial::Baud115200));
    Ok(())
  }).unwrap();

  let (led_command_tx, led_command_rx) = mpsc::channel();

  let mut led_strip_data = [[0, 0, 0u8]; N_LEDS as usize];

  let mut intro_outro_fade = animation::IntroOutroFade::new(
    time::PreciseTime::now(),
    time::Duration::seconds(5),
    time::PreciseTime::now(),
    time::Duration::seconds(7),

    N_LEDS);

  let mut last_pattern: Box<animation::PlasmaPattern> = Box::new(animation::Rainbow);
  let mut current_pattern: Box<animation::PlasmaPattern> = Box::new(animation::Rainbow);
  let mut pattern_fade_begin = time::PreciseTime::now();
  let pattern_fade_duration = time::Duration::seconds(3);

  wait_1_byte(&mut serial_port);

  intro_outro_fade.start_intro();

  {
    let led_command_tx = led_command_tx.clone();
    thread::spawn(move || {
      let mut client = hyper::client::Client::new();
      client.set_read_timeout(Some(std::time::Duration::from_secs(10)));

      loop {
        match client.get("http://www.msftncsi.com/ncsi.txt").send() {
          Ok(mut response) => {
            let mut response_text = String::new();
            response.read_to_string(&mut response_text);
            if response_text == "Microsoft NCSI" {
              led_command_tx.send(LedStripCommand::ChangePalette(Box::new(animation::Rainbow)));
              println!("Connected to internet");
            } else {
              led_command_tx.send(LedStripCommand::ChangePalette(Box::new(animation::RedWave)));
              println!("Not connected to internet");
            }
          },
          Err(_) => {
            led_command_tx.send(LedStripCommand::ChangePalette(Box::new(animation::RedWave)));
              println!("Not connected to internet");
          }
        }
        thread::sleep(std::time::Duration::from_secs(5));
      }
    });
  }

  {
    let led_command_tx = led_command_tx.clone();
    thread::spawn(move || {
      let mut client = hyper::client::Client::new();
      client.set_read_timeout(Some(std::time::Duration::from_secs(10)));

      loop {
        match client.get("http://10.0.1.2/pi_api/pir/?a=getStatus").send() {
          Ok(mut response) => {
            let mut response_text: String = String::new();
            response.read_to_string(&mut response_text);
            if let Ok(pir_status) = serde_json::from_str::<pir::PirStatus>(&response_text) {
              if let Ok(timestamp)= pir_status.timestamp.parse::<i64>() {
                if timestamp as u64 > std::time::UNIX_EPOCH.elapsed().unwrap().as_secs()-60*5 { //within last 5 minutes
                  led_command_tx.send(LedStripCommand::TurnOn);
                  println!("Movement detected, turn on");
                } else {
                  led_command_tx.send(LedStripCommand::TurnOff);
                  println!("No movement detected, turn off");
                }
              } else {
                println!("{:?} can't be parsed as i64!", pir_status.timestamp);
              }
            } else {
              println!("{:?} can't be parsed as json!", response_text);
            }
          },
          Err(_) => {
            println!("Couldn't contact pir api!");
          }
        }
        thread::sleep(std::time::Duration::from_secs(2));
      }
    });
  }

  loop {


    while let Ok(command) = led_command_rx.try_recv() {
      match command {
        LedStripCommand::TurnOn => {
          intro_outro_fade.start_intro();
        }

        LedStripCommand::TurnOff => {
          intro_outro_fade.start_outro();
        }

        LedStripCommand::ChangePalette(new_pattern) => {
          last_pattern = current_pattern;
          current_pattern = new_pattern;
          pattern_fade_begin = time::PreciseTime::now();
        }
      }
    }


    for led in 0..N_LEDS {
      let fade_factor = 0.0f32.max(1.0f32.min((pattern_fade_begin.to(time::PreciseTime::now()).num_milliseconds() as f32 / pattern_fade_duration.num_milliseconds() as f32)));

      let fade = intro_outro_fade.get_fade(led, time::PreciseTime::now());
      //let background_color = animation::background_pattern(&last_pattern, &current_pattern, fade_factor, led, start_time.to(time::PreciseTime::now()));
      
      let strip_color = animation::strip_pattern(&last_pattern, &current_pattern, fade_factor, led, start_time.to(time::PreciseTime::now()));

      led_strip_data[led as usize] = [
        (strip_color.0 * fade * 127.0) as u8,
        (strip_color.1 * fade * 127.0) as u8,
        (strip_color.2 * fade * 127.0) as u8,
      ];

      if led == 0 { assert!(serial_port.write(&[led_strip_data[led as usize][0] | 0b10000000]).unwrap() == 1); }
      else { assert!(serial_port.write(&[std::cmp::min(led_strip_data[led as usize][0], 127)]).unwrap() == 1); }
      assert!(serial_port.write(&[std::cmp::min(led_strip_data[led as usize][1], 127)]).unwrap() == 1);
      assert!(serial_port.write(&[std::cmp::min(led_strip_data[led as usize][2], 127)]).unwrap() == 1);
    }
    serial_port.flush().unwrap();
    wait_1_byte(&mut serial_port);
  }
}
